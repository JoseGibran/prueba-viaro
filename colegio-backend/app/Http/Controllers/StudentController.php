<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Student;

class StudentController extends Controller
{
    public function index(){
        return Student::all();
    }
    public function store(Request $request){
        $student = new Student;
        $student->name=$request->name;
        $student->lastname=$request->lastname;
        $student->genre=$request->genre;
        $student->birthdate=$request->birthdate;

        $student->save();

        return json_encode(true);
    }

    public function update(Request $request){
        $student = Student::find($request->id);
        $student->name=$request->name;
        $student->lastname=$request->lastname;
        $student->genre=$request->genre;
        $student->birthdate=$request->birthdate;
        $student->save();
        return json_encode(true);
    }

    public function delete(Request $request){

        $flight = Student::find($request->id);

        $flight->delete();

        return json_encode(true);
    }

}
