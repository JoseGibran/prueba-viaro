import React from 'react';
import firebase from 'firebase';
import Form from './Form';
import $ from 'jquery';

class Profesor extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: [
            ],
            selected: null,
        }
    }


    createRecord = (record) => {
        firebase.database().ref('profesors').push(record);

        this.refreshData();

    }

    deleteRecord = (ID) => {
        if(window.confirm('¿Está seguro que quiere elminar el registro?')){

            
            firebase.database().ref(`grades`).orderByChild('profesorId').equalTo(ID).once('value', snapshot=>{
                if( snapshot.val() === null){
                    firebase.database().ref(`profesors/${ID}`).remove();
                    this.refreshData();
                }else{
                    alert("Primero debe eliminar los cursos a los que este ingresado este profesor");
                }
            });

            
        }
    }

    editRecord = (record) => {
        this.setState({selected: record}, ()=>{
            $("#openModal").click();
        });
    }
    updateRecord = (record) => {
        const id = record.id;
        delete record.id;
        firebase.database().ref(`profesors/${id}`).update(record);
        this.refreshData();
    }
  
    _renderItem = (item, index) => (
        <tr key={`Profesor_${index}`}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.lastname}</td>
            <td>{item.genre}</td>
            <td>
                <button onClick={this.deleteRecord.bind(this, item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                <button onClick={this.editRecord.bind(this, item)} className="btn btn-sm btn-primary" style={{marginLeft: 5}}>Editar</button>
            </td>
        </tr>
    )

    refreshData = () => {
        firebase.database().ref('profesors').on('value', (snapshot)=> {
            if(snapshot.val()){
                let data = [];
                for(let id in snapshot.val() ){
                    data = [...data, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({data, selected: null});
            }
        });
    }

    componentDidMount(){
        this.refreshData();
    }
    
    render(){
        const {data, selected} = this.state;
        return(
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Género</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(this._renderItem)}
                    </tbody>
                </table>
                {selected ?
                    <Form id={selected.id} name={selected.name} lastname={selected.lastname} genre={selected.genre} birthdate={selected.birthdate} createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                :
                    <Form createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                }
                
            </div>
        );
    }

}

export default Profesor;