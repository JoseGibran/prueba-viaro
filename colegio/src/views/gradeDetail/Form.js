import React from 'react';
import $ from 'jquery';
import firebase from 'firebase';

class Form extends React.Component{

    static defaultProps = {
        createRecord: () => {},
        studentId: '',
        gradeId: '',
        section: ''
    }

    constructor(props){
        super(props);
        this.state = {
            record:{
                studentId: this.props.studentId,
                gradeId: this.props.gradeId,
                section: this.props.section,
            },
            students: [],
            grades:[],
        }
    }

    onChangeInput = (event) => {
        this.state.record[event.target.name] = event.target.value;
        this.setState({record: this.state.record});
    }

    save = (event) => {
        event.preventDefault();

        console.log(this.state);

        if(this.state.record.studentId.length > 0){
            if(this.state.record.gradeId.length > 0){
                if(this.props.id){
                    this.props.updateRecord({
                        id: this.props.id,
                        ...this.state.record,
                    });
                }else{
                    this.props.createRecord(this.state.record);
                }
            }else{
                alert("Debe elegir un Grado");
            }
            
        }else{
            alert("Debe elegir un Alumno");
        }
        
        $("#exampleModal .close").click();
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextState){
        this.setState({
            record:{
                studentId: nextProps.studentId || '',
                gradeId: nextProps.gradeId || '',
                section: nextProps.section || '',
            }
        });
    }

    componentDidMount(){
        firebase.database().ref('students').on('value', (snapshot) => {
            if(snapshot.val()){
                let students = [];
                for(let id in snapshot.val() ){
                    students = [...students, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({students});
            }
        });
        firebase.database().ref('grades').on('value', (snapshot) => {
            if(snapshot.val()){
                let grades = [];
                for(let id in snapshot.val() ){
                    grades = [...grades, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({grades});
            }
        });
    }
    

    

    render(){
        const {section, studentId, gradeId} = this.state.record;
        const {students, grades} = this.state;
        return(
            <div onClick={this.handleClick}>

                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="openModal">
                    Agregar
                </button>
                <form onSubmit={this.save}>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Agregar Registro</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                            
                                
                                <div className="form-group">
                                    <label htmlFor="studentId">Estudiante</label>
                                        <select value={studentId} onChange={this.onChangeInput} className="form-control" id="studentId" name="studentId" required>
                                                <option value="">Seleccione un estudiante</option>
                                            {students.map((item,index)=>(
                                                <option key={`student_${index}`} value={item.id}>{`${item.name} ${item.lastname}`}</option>
                                            ))}
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="gradeId">Grados</label>
                                        <select value={gradeId} onChange={this.onChangeInput} className="form-control" id="gradeId" name="gradeId" required>
                                                <option value="">Seleccione un grado</option>
                                            {grades.map((item,index)=>(
                                                <option key={`grade_${index}`} value={item.id}>{`${item.name}`}</option>
                                            ))}
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="name">Sección</label>
                                    <input value={section} onChange={this.onChangeInput} name="section" type="text" className="form-control"  placeholder="Sección" required />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <span type="button" className="btn btn-secondary" data-dismiss="modal">Close</span>
                                <input type="submit" className="btn btn-primary" value="Guardar" />
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
               
            </div>
        );
    }
}

export default Form;