import React from 'react';
import firebase from 'firebase';
import Form from './Form';
import $ from 'jquery';
import swal from 'sweetalert'

class GradeDetail extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: [
                //{id: 1, name:'G', lastname: 'P', genre: 'Hombre', birthdate: '13/04/1997'}
            ],
            grades: {},
            students: {},
            selected: null,
        }
    }


    createRecord = (record) => {

        const {data} = this.state;
        if(data.filter(x=>x.studentId===record.studentId).length === 0){
            firebase.database().ref('gradeDetails').push(record);
            this.refreshData();
        }else{
            alert("No puede ingresar un registro duplicado");
        }

        
    }

    deleteRecord = (ID) => {

        swal({
            title: "Eliminar",
            text: "Esta seguro que quiere borrar el registro?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willDelete => {
            if (willDelete) {
                firebase.database().ref(`gradeDetails/${ID}`).remove();
                firebase.database().ref(`gradeDetails`).once('value', snapshot => {
              
                })
                swal("Eliminado!", "El registro se ha borrado correctamente", "success");
            }
          });
    }

    editRecord = (record) => {
        this.setState({selected: record}, ()=>{
            $("#openModal").click();
        });
    }
    updateRecord = (record) => {
        const id = record.id;
        delete record.id;
        firebase.database().ref(`gradeDetails/${id}`).update(record);
        this.refreshData();
    }
  
    _renderItem = (item, index) => (
        <tr key={`grade_${index}`}>
            <td>{item.id}</td>
            <td>{`${this.state.students[item.studentId].name} ${this.state.students[item.studentId].lastname}`}</td>
            <td>{`${this.state.grades[item.gradeId].name}`}</td>
            <td>{item.section}</td>
            <td>
                <button onClick={this.deleteRecord.bind(this, item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                <button onClick={this.editRecord.bind(this, item)} className="btn btn-sm btn-primary" style={{marginLeft: 5}}>Editar</button>
            </td>
        </tr>
    )

    refreshData = () => {
        firebase.database().ref('gradeDetails').on('value', (snapshot)=> {
            if(snapshot.val()){
                let data = [];
                for(let id in snapshot.val() ){
                    data = [...data, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({data, selected: null});
            }else{
                this.setState({data: []});
            }
        });
        firebase.database().ref('students').on('value', (snapshot)=> {
            if(snapshot.val()){
                this.setState({students: snapshot.val()});
            }
        });
        firebase.database().ref('grades').on('value', (snapshot)=> {
            if(snapshot.val()){
                this.setState({grades: snapshot.val()});
            }
        });
    }

    componentDidMount(){
        this.refreshData();
    }
    
    render(){
        const {data, selected} = this.state;
        return(
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Alumno</th>
                            <th>Grado</th>
                            <th>Seccion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(this._renderItem)}
                    </tbody>
                </table>
                {selected ?
                    <Form id={selected.id} studentId={selected.studentId} gradeId={selected.gradeId}  section={selected.section} updateRecord={this.updateRecord}/>
                :
                    <Form createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                }
                
            </div>
        );
    }

}

export default GradeDetail;