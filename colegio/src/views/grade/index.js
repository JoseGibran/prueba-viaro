import React from 'react';
import firebase from 'firebase';
import Form from './Form';
import $ from 'jquery';

class Grade extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: [
                //{id: 1, name:'G', lastname: 'P', genre: 'Hombre', birthdate: '13/04/1997'}
            ],
            profesors: [],
            selected: null,
        }
    }


    createRecord = (record) => {
        firebase.database().ref('grades').push(record);

        this.refreshData();

    }

    deleteRecord = (ID) => {
        if(window.confirm('¿Está seguro que quiere elminar el registro?')){

            firebase.database().ref(`gradeDetails`).orderByChild('gradeId').equalTo(ID).once('value', snapshot=>{
                if(snapshot.val() === null){
                    firebase.database().ref(`grades/${ID}`).remove();
                    this.refreshData();
                }else{
                    alert("Primero debe ingresar los detalles de este curso");
                }
            });
        }
    }

    editRecord = (record) => {
        this.setState({selected: record}, ()=>{
            $("#openModal").click();
        });
    }
    updateRecord = (record) => {
        const id = record.id;
        delete record.id;
        firebase.database().ref(`grades/${id}`).update(record);
        this.refreshData();
    }
  
    _renderItem = (item, index) => (
        <tr key={`grade_${index}`}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{`${this.state.profesors[item.profesorId].name} ${this.state.profesors[item.profesorId].lastname}`}</td>
            <td>
                <button onClick={this.deleteRecord.bind(this, item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                <button onClick={this.editRecord.bind(this, item)} className="btn btn-sm btn-primary" style={{marginLeft: 5}}>Editar</button>
            </td>
        </tr>
    )

    refreshData = () => {
        firebase.database().ref('grades').on('value', (snapshot)=> {
            if(snapshot.val()){
                let data = [];
                for(let id in snapshot.val() ){
                    data = [...data, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({data, selected: null});
            }
        });
        firebase.database().ref('profesors').on('value', (snapshot)=> {
            if(snapshot.val()){
                this.setState({profesors: snapshot.val(), selected: null});
            }
        });

    }

    componentDidMount(){
        this.refreshData();
    }
    
    render(){
        const {data, selected} = this.state;
        return(
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Profesor</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(this._renderItem)}
                    </tbody>
                </table>
                {selected ?
                    <Form id={selected.id} name={selected.name} profesorId={selected.profesorId} updateRecord={this.updateRecord}/>
                :
                    <Form createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                }
                
            </div>
        );
    }

}

export default Grade;