import React from 'react';
import $ from 'jquery';
import firebase from 'firebase';

class Form extends React.Component{

    static defaultProps = {
        createRecord: () => {},
        name: '',
        profesorId: '',
    }

    constructor(props){
        super(props);
        this.state = {
            record:{
                name: this.props.name,
                profesorId: this.props.profesorId,
            },
            profesors: [],
        }
    }

    onChangeInput = (event) => {
        this.state.record[event.target.name] = event.target.value;
        this.setState(this.state);
    }

    save = (event) => {
        event.preventDefault();
        if(this.state.record.profesorId.length > 0){
            if(this.props.id){
                this.props.updateRecord({
                    id: this.props.id,
                    ...this.state.record,
                });
            }else{
                this.props.createRecord(this.state.record);
            }
        }else{
            alert("Debe elegir un profesor");
        }
        
        $("#exampleModal .close").click();
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextState){
        this.setState({
            record:{
                name: nextProps.name || '',
                profesorId: nextProps.profesorId || '',
            }
        });
    }

    componentDidMount(){
        firebase.database().ref('profesors').on('value', (snapshot) => {
            if(snapshot.val()){
                let profesors = [];
                for(let id in snapshot.val() ){
                    profesors = [...profesors, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({profesors});
            }
        });
    }
    

    

    render(){
        const {name, profesorId} = this.state.record;
        const {profesors} = this.state;
        return(
            <div onClick={this.handleClick}>

                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="openModal">
                    Agregar
                </button>
                <form onSubmit={this.save}>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Agregar Registro</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                            
                                <div className="form-group">
                                    <label htmlFor="name">Nombre</label>
                                    <input value={name} onChange={this.onChangeInput} name="name" type="text" className="form-control"  placeholder="Nombre" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="profesorId">Profesor</label>
                                        <select value={profesorId} onChange={this.onChangeInput} className="form-control" id="profesorId" name="profesorId">
                                                <option value="">Seleccione un profesor</option>
                                            {profesors.map((item,index)=>(
                                                <option key={`profesor_${index}`} value={item.id}>{`${item.name} ${item.lastname}`}</option>
                                            ))}
                                    </select>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <span type="button" className="btn btn-secondary" data-dismiss="modal">Close</span>
                                <input type="submit" className="btn btn-primary" value="Guardar" />
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
               
            </div>
        );
    }
}

export default Form;