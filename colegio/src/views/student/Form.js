import React from 'react';
import $ from 'jquery';

class Form extends React.Component{

    static defaultProps = {
        createRecord: () => {},
        name: '',
        lastname: '',
        genre: 'Masculino',
        birthdate: '',
    }

    constructor(props){
        super(props);
        this.state = {
            name: this.props.name,
            lastname: this.props.lastname,
            genre: 'Masculino',
            birthdate: this.props.birthdate,
        }
    }

    onChangeInput = (event) => {
        this.state[event.target.name] = event.target.value;
        this.setState(this.state);
    }

    save = (event) => {
        event.preventDefault();
        if(this.props.id){
            this.props.updateRecord({
                id: this.props.id,
                ...this.state
            });
        }else{
            this.props.createRecord(this.state);
        }
        $("#exampleModal .close").click()
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextState){
        this.setState({
            name: nextProps.name || '',
            lastname: nextProps.lastname || '',
            genre: nextProps.genre || 'Masculino',
            birthdate: nextProps.birthdate || ''
        });
    }
    

    

    render(){

        const {name, lastname, genre, birthdate} = this.state;

        return(
            <div onClick={this.handleClick}>

                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="openModal">
                    Agregar
                </button>
                <form onSubmit={this.save}>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Agregar Registro</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                            
                                <div className="form-group">
                                    <label htmlFor="name">Nombre</label>
                                    <input value={name} onChange={this.onChangeInput} name="name" type="text" className="form-control"  placeholder="Nombre" required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastname">Apellidos</label>
                                    <input value={lastname} onChange={this.onChangeInput} name="lastname" type="text" className="form-control"  placeholder="Apellido" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="genre">Género</label>
                                        <select value={genre} onChange={this.onChangeInput} className="form-control" id="genre" name="genre">
                                        <option>Masculino</option>
                                        <option>Femenino</option>
                                        <option>Otro</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="birthdate">Fecha de Nacimiento</label>
                                    <input value={birthdate} onChange={this.onChangeInput} name="birthdate" type="date" className="form-control"  placeholder="Fecha de Nacimiento" required/>
                                </div>

                            </div>
                            <div className="modal-footer">
                                <span type="button" className="btn btn-secondary" data-dismiss="modal">Close</span>
                                <input  type="submit" className="btn btn-primary" value="Guardar" />
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
               
            </div>
        );
    }
}

export default Form;