import React from 'react';
import firebase from 'firebase';
import Form from './Form';
import $ from 'jquery';

class Student extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: [
                //{id: 1, name:'G', lastname: 'P', genre: 'Hombre', birthdate: '13/04/1997'}
            ],
            selected: null,
        }
    }


    createRecord = (record) => {
        //firebase.database().ref('students').push(record);

        let body = new FormData();
        body.append("name", record.name);
        body.append("lastname", record.lastname);
        body.append("genre", record.genre);
        body.append("birthdate", record.birthdate);

        fetch('http://localhost:8000/', {
            method: 'POST',
            body: body,
        }).then((raw) => raw.json()).then((response)=>{
    
            
        });


        this.refreshData();

    }

    deleteRecord = (ID) => {
        let body = new FormData();
        body.append("id", ID);
        if(window.confirm('¿Está seguro que quiere elminar el registro?')){
           /* firebase.database().ref(`gradeDetails`).orderByChild('studentId').equalTo(ID).once('value', snapshot=>{
                if(snapshot.val() === null){
                    firebase.database().ref(`students/${ID}`).remove();
                    this.refreshData();
                }else{
                    alert("Primero debe eliminar los cursos a los que este ingresado este alumno");
                }
            });*/
            fetch('http://localhost:8000/delete', {
                method: 'POST',
                body: body,
            }).then((raw) => raw.json()).then((response)=>{
                this.refreshData();
            });

        }
    }

    editRecord = (record) => {
        this.setState({selected: record}, ()=>{
            $("#openModal").click();
        });
    }
    updateRecord = (record) => {
        let body = new FormData();
        body.append("id", record.id);
        body.append("name", record.name);
        body.append("lastname", record.lastname);
        body.append("genre", record.genre);
        body.append("birthdate", record.birthdate);


        fetch('http://localhost:8000/update', {
            method: 'POST',
            body: body,
        }).then((raw) => raw.json()).then((response)=>{
            this.refreshData();
        });

        //firebase.database().ref(`students/${id}`).update(record);
        this.refreshData();
    }
  
    _renderItem = (item, index) => (
        <tr key={`student_${index}`}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.lastname}</td>
            <td>{item.genre}</td>
            <td>{item.birthdate}</td>
            <td>
                <button onClick={this.deleteRecord.bind(this, item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                <button onClick={this.editRecord.bind(this, item)} className="btn btn-sm btn-primary" style={{marginLeft: 5}}>Editar</button>
            </td>
        </tr>
    )

    refreshData = () => {

        fetch('http://localhost:8000/', ).then((raw) => raw.json()).then((response)=>{
            if(response){
                this.setState({data: response});
            }
            
        });

       /*firebase.database().ref('students').on('value', (snapshot)=> {
            if(snapshot.val()){
                let data = [];
                for(let id in snapshot.val() ){
                    data = [...data, {
                        id,
                        ...snapshot.val()[id]
                    }];
                }
                this.setState({data, selected: null});
            }
        });*/
    }

    componentDidMount(){
        this.refreshData();
    }
    
    render(){
        const {data, selected} = this.state;
        return(
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Género</th>
                            <th>Fecha nacimiento</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(this._renderItem)}
                    </tbody>
                </table>
                {selected ?
                    <Form id={selected.id} name={selected.name} lastname={selected.lastname} genre={selected.genre} birthdate={selected.birthdate} createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                :
                    <Form createRecord={this.createRecord} updateRecord={this.updateRecord}/>
                }
                
            </div>
        );
    }

}

export default Student;