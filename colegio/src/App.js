import React, { Component } from 'react';
import logo from './logo.svg';
import { Route, Switch, Redirect } from 'react-router-dom';

import './App.css';
import firebase from 'firebase';
import Student from './views/student'
import Profesor from './views/profesor';
import Grade from './views/grade';
import GradeDetail from './views/gradeDetail'

var config = {
  apiKey: "AIzaSyB_wtuGWhOskIinq1Z_rgQYL5OlhxU5JZc",
  authDomain: "colegio-cdd04.firebaseapp.com",
  databaseURL: "https://colegio-cdd04.firebaseio.com",
  projectId: "colegio-cdd04",
  storageBucket: "colegio-cdd04.appspot.com",
  messagingSenderId: "119775487521"
};
var app = firebase.initializeApp(config);

class App extends Component {

  

  render() {
    return (
     <div >
      


        <div className="container">

          <div id="navbar" className="navbar-collapse collapse">
            <ul className="nav navbar-nav">
              <li className="active">
              <a href="/students">Estudiantes</a></li>
              <li><a href="/profesors">Profesores</a></li>
              <li><a href="/grades">Grados</a></li>
              <li><a href="/gradedetail">Detalle Curso</a></li>
            </ul>
          </div>


          <Switch>
              <Route path="/students" component={Student} />
              <Route path="/profesors" component={Profesor} />
              <Route path="/grades" component={Grade} />
              <Route path="/gradedetail" component={GradeDetail} />
              <Route path="/" exact render={() => (<Redirect to="/students" />)} />
          </Switch>
        </div>
        
     </div>
     
    );
  }
}

export default App;
